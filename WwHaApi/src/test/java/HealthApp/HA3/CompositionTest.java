package HealthApp.HA3;

import org.junit.Before;
import org.junit.Test;

import junit.framework.Assert;
import measurement.Compositions;

public class CompositionTest {
	private Compositions c;
	
	@Before
	public void setUp() throws Exception{
		c = new Compositions();
	}
	
	@Test
	public void testComposition () {
		Assert.assertSame("You are underweight. (0..18.5)", c.Composition(15));
	}
}
