package HealthApp.HA3;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


import measurement.converterBMI;

public class ConvertBMITest {
	private converterBMI cbmi;
	
	@Before
	public void setUp() throws Exception {
		cbmi = new converterBMI();
	}
	@Test
	public void testConvertBMI () {
		
		// Given
		
		// When
		double result = cbmi.convertBMI(1.80, 60);		
		
		// Then
		Assert.assertEquals(18.52, result, 0.05);
	}
}
