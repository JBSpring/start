package measurement;

public class Compositions {
	
	
	
	public  String Composition(double bmi) {

		if (bmi < 18.5) {
			return "You are underweight. (0..18.5)";
		} else {
			if (bmi > 25) {
				return "You are overweight. (25..)";
			} else {
				return "Your weight is normal. (18.5..25)";
			}
		}
	} 
}